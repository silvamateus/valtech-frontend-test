const fractal = module.exports = require('@frctl/fractal').create()

fractal.set('project.title', 'test Valtech')
fractal.web.set('builder.dest', '/dist') 
fractal.web.set('static.path',` ${__dirname}/dist`)
fractal.docs.set('path', `${__dirname}/src/docs`) 
fractal.components.set('path', `${__dirname}/dist`)
fractal.components.set('ext', `.html`)
