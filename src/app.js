import Vue from 'vue'
import App from './App.vue'
import 'normalize.css'
import './resources/sass/default.scss'

new Vue ({
    render: h => h(App),
    el: '#app'
})