import mainImg from '../img/Lagoa.jpg'
import amsterdam from '../img/Amsterdam.jpg'
import barcelona from '../img/Barcelona.jpg'
import berlin from '../img/Berlin.jpg'
import london from '../img/London.jpg'
import paris from '../img/Paris.jpg'
import venice from '../img/Venice.jpg'

const container = {
  logo: './img/Valtech-logo.eps',
  header: {
    description: 'FRONT-END',
    title: 'valtech_',
    info: 'Lorem ipsum orci sit ultricies non elit curabitur in risus felis, pretium mi sem quam aptent semper curae sollicitudin.', 
    background: mainImg
  },
  'places': [
    {
      country: 'Italy',
      state: 'Venice',
      info: 'Venice, the capital of northern Italy’s Veneto region, is built on more than 100 small islands in a lagoon in the Adriatic Sea. It has no roads, just canals.',
      background: venice
    },
    {
      country: 'Germany',
      state: 'Berlin',
      info: 'Berlin is the capital and the largest city of Germany, as well as one of its 16 constituent states.',
      background: berlin
    },
    {
      country: 'Spain',
      state: 'Barcelona',
      info: 'Barcelona is a city in Spain. It is the capital and largest city of Catalonia, as well as the second most populous municipality of Spain.',
      background: barcelona
    },
    {
      country: 'France',
      state: 'Paris',
      info: 'Paris is the capital and most populous city of France.',
      background: paris
    },
    {
      country: 'Netherlands',
      state: 'Amsterdam',
      info: 'Amsterdam name derives from Amstelredamme, indicative of the city\'s origin around a dam in the river Amstel.',
      background: amsterdam
    },
    {
      country: 'United Kingdom',
      state: 'London',
      info: 'London has a diverse range of people and cultures, and more than 300 languages are spoken in the region.',
      background: london
    }
  ]
}

export default container