const path = require('path')
const gulp = require('gulp')
const compiler = require('webpack')
const webpack = require('webpack-stream')
const devServer = require('webpack-dev-server')

gulp.task('serve', callback => {
    process.env.NODE_ENV = 'development'
    const webpackConfig = require('./webpack.config.js')
    const config = {
        ...webpackConfig,
        entry: {
            app: [
                'webpack-dev-server/client?http://localhost:'+webpackConfig.devServer.port,
                'webpack/hot/dev-server',
                ...webpackConfig.entry.app
            ]
        },
        devtool: 'source-map-eval',
    }
    const server = new devServer(compiler(config), config.devServer)
    server.listen(config.devServer.port, 'localhost', function() {
        console.log('listening on port', config.devServer.port)
    })
})

gulp.task('build', () => {
    process.env.NODE_ENV = 'production'
    const config = require('./webpack.config.js')
    
    return  gulp.src('./src/app.js')
            .pipe(webpack(config))
            .pipe(gulp.dest('dist/'))
})

gulp.task('default', () => {
    
})
