const app = require('express')()
const path = require('path')
const static = require('serve-static')
const port = process.env.PORT || 8000

app.use(static(path.resolve(__dirname, 'dist')))
app.listen(port, () => console.log('listenning on port :8080'))